/*
 * %W% %E% staff-help - asus
 *
 * Copyright (c) 2017-2018 SMAC VNIT, jsc. All Rights Reserved. 
 *
 * This software is the confidential and proprietary information of SMAC VNIT, jsc. 
 * ("Confidential Information"). You shall not disclose such Confidential Information 
 * and shall use it only in accordance with the terms of the license agreement you entered 
 * into with SMAC.
 *
 * SMAC MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF 
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SMAC SHALL NOT BE LIABLE FOR 
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR 
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/
package com.staffhelp.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Shop Entity.
 * 
 * @since		10:35:49 AM
 * @author		asus
 */
@Entity
@Data
@Table(name = "shop")
@AllArgsConstructor
@NoArgsConstructor
public class ShopEntity {
	@Id
	@Column(name = "shop_id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer shopId;
	
	@Column(name = "shop_code",length = 50)
	private String shopCode;
	
	@Column(name = "shop_name",length = 50)
	private String shopName;
	
	@Column(length = 20)
	private String tel;
	
	@Column(length = 50)
	private String address;
	
	@Column(name = "shop_path",length = 50)
	private String shopPath;
	
	@Column(length = 50)
	private String status;
	
	@Temporal(TemporalType.TIMESTAMP)	
	private Date createdDate;
	
	@OneToMany(mappedBy = "shop")
	private List<StaffEntity> staffEntities = new ArrayList<StaffEntity>();

	public Integer getShopId() {
		return shopId;
	}

	public void setShopId(Integer shopId) {
		this.shopId = shopId;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getShopPath() {
		return shopPath;
	}

	public void setShopPath(String shopPath) {
		this.shopPath = shopPath;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public List<StaffEntity> getStaffEntities() {
		return staffEntities;
	}

	public void setStaffEntities(List<StaffEntity> staffEntities) {
		this.staffEntities = staffEntities;
	}
	
}

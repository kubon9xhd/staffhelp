package com.staffhelp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StaffHelpApplication {

	public static void main(String[] args) {
		SpringApplication.run(StaffHelpApplication.class, args);
	}

}

/*
 * %W% %E% staff-help - asus
 *
 * Copyright (c) 2017-2018 SMAC VNIT, jsc. All Rights Reserved. 
 *
 * This software is the confidential and proprietary information of SMAC VNIT, jsc. 
 * ("Confidential Information"). You shall not disclose such Confidential Information 
 * and shall use it only in accordance with the terms of the license agreement you entered 
 * into with SMAC.
 *
 * SMAC MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF 
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SMAC SHALL NOT BE LIABLE FOR 
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR 
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/
package com.staffhelp.service;

import java.util.List;

import com.staffhelp.dto.StaffDTO;

/**
 * Class description goes here.
 * 
 * @since		10:49:10 AM
 * @author		asus
 */
public interface IStaffService {
	List<StaffDTO> findAll(String query);
	StaffDTO save(StaffDTO staffDTO);
	StaffDTO update(StaffDTO staffDTO);
	Boolean remove(Integer id);

}

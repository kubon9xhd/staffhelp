/*
 * %W% %E% staff-help - asus
 *
 * Copyright (c) 2017-2018 SMAC VNIT, jsc. All Rights Reserved. 
 *
 * This software is the confidential and proprietary information of SMAC VNIT, jsc. 
 * ("Confidential Information"). You shall not disclose such Confidential Information 
 * and shall use it only in accordance with the terms of the license agreement you entered 
 * into with SMAC.
 *
 * SMAC MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF 
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SMAC SHALL NOT BE LIABLE FOR 
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR 
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/
package com.staffhelp.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.staffhelp.dto.HelpDTO;
import com.staffhelp.entity.HelpEntity;
import com.staffhelp.entity.StaffEntity;
import com.staffhelp.exception.ApiRequestException;
import com.staffhelp.repo.HelpRepository;
import com.staffhelp.repo.IStaffRepository;
import com.staffhelp.service.IHelpService;
import com.staffhelp.specification.SpecificationsBuilderCustom;
import com.staffhelp.ultils.Constanst;

/**
 * .
 * 
 * @since 2:08:09 PM
 * @author asus
 */
@Service
public class HelpService implements IHelpService {

	@Autowired
	HelpRepository helpRepo;

	@Autowired
	IStaffRepository staffRepo;

	/*
	 * method to handling logics and exceptions to find all by query
	 */
	@Override
	public List<HelpDTO> findAll(String query) {
		SpecificationsBuilderCustom<HelpEntity> builder = new SpecificationsBuilderCustom<HelpEntity>();
		Pattern pattern = Pattern.compile(Constanst.QUERY_PATTERN);
		Matcher matcher = pattern.matcher(query + ",");
		Boolean isStatus = false;
		// loop until to the last element of matcher
		while (matcher.find()) {
			// check if is exist key status and change value
			if (matcher.group(1).equals("status")) {
				isStatus = true;
			}
			builder.with(matcher.group(1), matcher.group(2), matcher.group(3));
		}
		// check if exist status
		if (isStatus) {
			Specification<HelpEntity> specification = builder.build();
			List<HelpEntity> helpEntities = helpRepo.findAll(specification);
			ModelMapper modelMapper = new ModelMapper();
			List<HelpDTO> helpDTOs = new ArrayList<HelpDTO>();
			// loop until to the last element of list
			for (HelpEntity helpEntity : helpEntities) {
				helpDTOs.add(modelMapper.map(helpEntity, HelpDTO.class));
			}
			return helpDTOs;
		} else {
			throw new ApiRequestException("status field is missing.");
		}
	}

	/*
	 * method to add new help object
	 */
	@Override
	public HelpDTO save(HelpDTO helpDTO) {
		StaffEntity staffEntity;
		// check if object does not created user
		if (helpDTO.getCreatedUser() != null) {
			staffEntity = staffRepo.findById(helpDTO.getCreatedUser())
					.orElseThrow(() -> new ApiRequestException("Staff does not exist"));
		} else {
			throw new ApiRequestException("Please input createdUser.");
		}
		ModelMapper modelMapper = new ModelMapper();

		HelpEntity helpEntity = modelMapper.map(helpDTO, HelpEntity.class);
		helpEntity.setStaff(staffEntity);
		HelpDTO helpDtoResponse = modelMapper.map(helpRepo.save(helpEntity), HelpDTO.class);

		return (helpDtoResponse != null) ? helpDtoResponse : null;
	}

	/*
	 * method to update help entity
	 */
	@Override
	@Transactional
	public HelpDTO update(HelpDTO helpDTO) {
		HelpEntity helpEntity = helpRepo.findById(helpDTO.getHelpId())
				.orElseThrow(() -> new ApiRequestException("Can not found ID") // check if can not found entity by id
				);
		StaffEntity staffEntity = staffRepo.findById(helpDTO.getCreatedUser())
				.orElseThrow(() -> new ApiRequestException("Staff does not exist") // check if can not found staff by
																					// id created user
				);
		// check if entity not active
		if (helpEntity.getStatus().equals("0")) {
			throw new ApiRequestException("Can not update the Help is not active");
		}
		ModelMapper modelMapper = new ModelMapper();
		helpEntity = modelMapper.map(helpDTO, HelpEntity.class);
		helpEntity.setStaff(staffEntity);

		helpRepo.save(helpEntity);
		helpDTO = modelMapper.map(helpEntity, HelpDTO.class);

		return (helpDTO != null) ? helpDTO : null;
	}
	/*
	 * method to update status of object change to 1 - 0
	 * */
	@Override
	public Boolean remove(Integer id) {
		helpRepo.findById(id)
		.orElseThrow(
				() -> new ApiRequestException("Can not delete ID:" + id) // if can not found object 
																		// by id
		);

		Optional<HelpEntity> helpOptional = helpRepo.findById(id);
		// check if helpobject is present
		if (helpOptional.isPresent()) {
			helpOptional.get().setStatus("0");
			helpRepo.save(helpOptional.get());
			return true;
		} else {
			return false;
		}
	}

}

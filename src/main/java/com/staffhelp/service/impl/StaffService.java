/*
 * %W% %E% staff-help - asus
 *
 * Copyright (c) 2017-2018 SMAC VNIT, jsc. All Rights Reserved. 
 *
 * This software is the confidential and proprietary information of SMAC VNIT, jsc. 
 * ("Confidential Information"). You shall not disclose such Confidential Information 
 * and shall use it only in accordance with the terms of the license agreement you entered 
 * into with SMAC.
 *
 * SMAC MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF 
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SMAC SHALL NOT BE LIABLE FOR 
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR 
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/
package com.staffhelp.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.staffhelp.dto.StaffDTO;
import com.staffhelp.entity.ShopEntity;
import com.staffhelp.entity.StaffEntity;
import com.staffhelp.exception.ApiRequestException;
import com.staffhelp.repo.IStaffRepository;
import com.staffhelp.repo.ShopRepository;
import com.staffhelp.service.IStaffService;
import com.staffhelp.specification.SpecificationsBuilderCustom;
import com.staffhelp.ultils.Constanst;

/**
 * Class description goes here.
 * 
 * @since 2:08:27 PM
 * @author asus
 */
@Service
public class StaffService implements IStaffService {

	@Autowired
	private IStaffRepository staffRepo;
	@Autowired
	private ShopRepository shopRepo;

	/*
	 * method to find all staff by criteria
	 */
	@Override
	public List<StaffDTO> findAll(String query) {
		SpecificationsBuilderCustom<StaffEntity> builder = new SpecificationsBuilderCustom<StaffEntity>();
		Pattern pattern = Pattern.compile(Constanst.QUERY_PATTERN);
		Matcher matcher = pattern.matcher(query + ",");
		Boolean isStatus = false;
		// loop until to the last element of matcher can find
		while (matcher.find()) {
			// check if is equal status and change flag
			if (matcher.group(1).equals("status")) {
				isStatus = true;
			}
			builder.with(matcher.group(1), matcher.group(2), matcher.group(3));
		}
		// check if flag is true
		if (isStatus) {
			Specification<StaffEntity> specification = builder.build();
			List<StaffEntity> staffEntities = staffRepo.findAll(specification);
			ModelMapper modelMapper = new ModelMapper();
			List<StaffDTO> staffDTOs = new ArrayList<StaffDTO>();
			// loop until to the last element
			for (StaffEntity staffEntity : staffEntities) {
				staffDTOs.add(modelMapper.map(staffEntity, StaffDTO.class));
			}
			return staffDTOs;
		} else {
			throw new ApiRequestException("status field is missing.");
		}
	}

	/*
	 * method to add new staff enity
	 */
	@Override
	public StaffDTO save(StaffDTO staffDTO) {
		// check if name of staff null or empty
		if (staffDTO.getStaffName() == null || staffDTO.getStaffName().isEmpty()) {
			throw new ApiRequestException("Name staff can not empty");
		}
		ModelMapper modelMapper = new ModelMapper();
		StaffEntity staffEntity = modelMapper.map(staffDTO, StaffEntity.class);
		staffEntity.setStatus("1");
		StaffDTO staffDtoRes = modelMapper.map(staffRepo.save(staffEntity), StaffDTO.class);
		return (staffDtoRes != null) ? staffDtoRes : null;
	}

	@Override
	@Transactional
	public StaffDTO update(StaffDTO staffDTO) {
		StaffEntity staffEntity = staffRepo.findById(staffDTO.getStaffId())
				.orElseThrow(() -> new ApiRequestException("Staff id does not exist") // check if does not exist
																						// staff by id
				);
		// check if the staff does not active
		if (staffEntity.getStatus().equals("0")) {
			throw new ApiRequestException("Can not update the Staff is not active"); 
		}

		ModelMapper modelMapper = new ModelMapper();
		staffEntity = modelMapper.map(staffDTO, StaffEntity.class);
		// check staff if shop id not null
		if (staffDTO.getShopId() != null) {
			ShopEntity shopEntity = shopRepo.findById(staffDTO.getShopId())
					.orElseThrow(
							() -> new ApiRequestException("Shop id does not exist") // check if can not found 
																					// shop by id
							);
			staffEntity.setShop(shopEntity);
		}
		StaffDTO stafDtoRes = modelMapper.map(staffRepo.save(staffEntity), StaffDTO.class);
		return (stafDtoRes != null) ? stafDtoRes : null;
	}

	@Override
	public Boolean remove(Integer id) {
		StaffEntity staffEntity = staffRepo.findById(id)
				.orElseThrow(
						() -> new ApiRequestException("Can not found Staff") // check if can not 
																			//found staff by id
						);
		staffEntity.setStatus("0");
		staffRepo.save(staffEntity);
		return true;
	}

}

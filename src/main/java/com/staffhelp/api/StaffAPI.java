/*
 * %W% %E% staff-help - asus
 *
 * Copyright (c) 2017-2018 SMAC VNIT, jsc. All Rights Reserved. 
 *
 * This software is the confidential and proprietary information of SMAC VNIT, jsc. 
 * ("Confidential Information"). You shall not disclose such Confidential Information 
 * and shall use it only in accordance with the terms of the license agreement you entered 
 * into with SMAC.
 *
 * SMAC MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF 
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SMAC SHALL NOT BE LIABLE FOR 
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR 
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/
package com.staffhelp.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.staffhelp.dto.StaffDTO;
import com.staffhelp.exception.ApiRequestException;
import com.staffhelp.service.IStaffService;

/**
 * Class description goes here.
 * 
 * @since 10:48:28 AM
 * @author asus
 */
@RestController
@RequestMapping("/v1/staffs")
public class StaffAPI {
	@Autowired
	private IStaffService staffService;

	/*
	 * method get criteria to search
	 **/
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<StaffDTO>> searchHelps(@RequestParam(value = "search") String search) {
		// check if query null or empty
		if (search == null || search.trim().isEmpty()) {
			throw new ApiRequestException("Does not have query to search");
		}
		return new ResponseEntity<List<StaffDTO>>(staffService.findAll(search), HttpStatus.OK);
	}

	/*
	 * method add new staff
	 */
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<StaffDTO> saveStaff(@RequestBody(required = false) StaffDTO staffDTO) {
		// check if object is null
		if (staffDTO != null) {
			// check if does not have staff id
			if (staffDTO.getStaffId() == null) {
				StaffDTO staffDtoRes = staffService.save(staffDTO);
				// check if save data fail
				if (staffDtoRes == null) {
					throw new ApiRequestException("Save staff fail");
				}
				return new ResponseEntity<>(staffDtoRes, HttpStatus.OK);
			} else {
				throw new ApiRequestException("Add new Staff does not have staffid");
			}
		} else {
			throw new ApiRequestException("Please, enter right input parameter");
		}
	}

	/*
	 * Method update Staff
	 */
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<StaffDTO> updateStaff(@RequestBody(required = false) StaffDTO staffDTO) {
		// check if staff DTO is null
		if (staffDTO != null) {
			// check if id is null
			if (staffDTO.getStaffId() == null) {
				throw new ApiRequestException("Staff must have id");
			}
			StaffDTO staffDtoResDto = staffService.update(staffDTO);
			// check if update done
			if (staffDtoResDto != null) {
				return new ResponseEntity<StaffDTO>(staffDtoResDto, HttpStatus.OK);
			} else {
				throw new ApiRequestException("Update staff fail");
			}
		} else {
			throw new ApiRequestException("Please, enter right input parameter");
		}
	}

	/*
	 * Method remove saff by id
	 */
	@RequestMapping(method = RequestMethod.DELETE)
	public ResponseEntity<Boolean> saveStaff(@RequestParam(value = "id") Integer id) {
		// check if id is null
		if (id != null) {
			Boolean isDone = staffService.remove(id);
			// check if status of isDone is true
			if (isDone) {
				return new ResponseEntity<>(isDone, HttpStatus.OK);
			} else {
				throw new ApiRequestException("Fail, id does not exist.");
			}
		} else {
			throw new ApiRequestException("Fail, Please delete must has id of object.");
		}
	}
}

/*
 * %W% %E% staff-help - asus
 *
 * Copyright (c) 2017-2018 SMAC VNIT, jsc. All Rights Reserved. 
 *
 * This software is the confidential and proprietary information of SMAC VNIT, jsc. 
 * ("Confidential Information"). You shall not disclose such Confidential Information 
 * and shall use it only in accordance with the terms of the license agreement you entered 
 * into with SMAC.
 *
 * SMAC MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF 
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SMAC SHALL NOT BE LIABLE FOR 
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR 
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/
package com.staffhelp.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.staffhelp.dto.HelpDTO;
import com.staffhelp.entity.StaffEntity;
import com.staffhelp.exception.ApiRequestException;
import com.staffhelp.service.IHelpService;

/**
 * Class description goes here.
 * 
 * @since 10:48:39 AM
 * @author asus
 */
@RestController
@RequestMapping("/v1/helps")
public class HelpAPI {

	/*
	 * call to Help service to logic processing
	 */
	@Autowired
	private IHelpService helpService;

	/*
	 * method to get list by criteria
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<StaffEntity>> searchHelps(@RequestParam(value = "search") String search) {
		// check if query to search is null or empty
		if (search == null || search.trim().isEmpty()) {
			throw new ApiRequestException("Does not have query to search");
		}
		return null;
	}

	/*
	 * method to save object Help and validate input
	 */
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<HelpDTO> saveHelp(@RequestBody(required = false) HelpDTO helpDTO) {
		// check if object not null
		if (helpDTO != null) {
			HelpDTO helpDtoResponse = helpService.save(helpDTO);
			// check if does not have id in object 
			if (helpDtoResponse.getHelpId() != null) {
				return new ResponseEntity<>(helpDtoResponse, HttpStatus.OK);
			} else {
				throw new ApiRequestException("Save help fail.");
			}
		} else {
			throw new ApiRequestException("Please, enter right input parameter ");
		}
	}

	/*
	 * method to update object Help by id and validate input
	 */
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<HelpDTO> updateHelp(@RequestBody(required = false) HelpDTO helpDTO) {
		// check if object is exist
		if (helpDTO != null) {
			// check if exist id in object to update
			if (helpDTO.getHelpId() == null) {
				throw new ApiRequestException("Fail, Please update must has id of object.");
			}
			HelpDTO helpDtoResponse = helpService.update(helpDTO);
			// check if action update to done
			if (helpDtoResponse != null) {
				return new ResponseEntity<>(helpDTO, HttpStatus.OK);
			} else {
				throw new ApiRequestException("Update help fail.");
			}
		} else {
			throw new ApiRequestException("Please, enter right input parameter ");
		}
	}

	/*
	 * method to delete object Help and validate input
	 */
	@RequestMapping(method = RequestMethod.DELETE)
	public ResponseEntity<Boolean> deleteHelp(@RequestParam(name = "id", required = false) Integer id) {
		// check if id is null
		if (id != null) {
			Boolean isDone = helpService.remove(id);
			// check if remove id done
			if (isDone) {
				return new ResponseEntity<>(isDone, HttpStatus.OK);
			} else {
				throw new ApiRequestException("Fail, id does not exist.");
			}
		} else {
			throw new ApiRequestException("Fail, Please delete must has id of object.");
		}
	}

}

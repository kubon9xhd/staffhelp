/*
 * %W% %E% staff-help - asus
 *
 * Copyright (c) 2017-2018 SMAC VNIT, jsc. All Rights Reserved. 
 *
 * This software is the confidential and proprietary information of SMAC VNIT, jsc. 
 * ("Confidential Information"). You shall not disclose such Confidential Information 
 * and shall use it only in accordance with the terms of the license agreement you entered 
 * into with SMAC.
 *
 * SMAC MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF 
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SMAC SHALL NOT BE LIABLE FOR 
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR 
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/
package com.staffhelp.endpoint.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.staffhelp.endpoint.service.IHelpEndpointService;
import com.staffhelp.entity.HelpEntity;
import com.staffhelp.entity.StaffEntity;
import com.staffhelp.repo.HelpRepository;
import com.staffhelp.repo.IStaffRepository;
import com.staffhelp.specification.SpecificationsBuilderCustom;
import com.staffhelp.ultils.Constanst;

import io.spring.guides.gs_producing_web_service.GetHelpsResponse;
import io.spring.guides.gs_producing_web_service.Help;
import io.spring.guides.gs_producing_web_service.HelpResponse;
import io.spring.guides.gs_producing_web_service.HelpResult;
import io.spring.guides.gs_producing_web_service.RemoveHelp;

/**
 * Help Service
 * 
 * @since 10:58:26 AM
 * @author asus
 */
@Service
public class HelpEndpointService implements IHelpEndpointService {

	@Autowired
	private HelpRepository helpRepo;

	@Autowired
	private IStaffRepository staffRepo;

	@Override
	public GetHelpsResponse findAll(String query) {
		// check if query is empty or null
		if(query == null || query.trim().isEmpty()) {
			return null;
		}
		SpecificationsBuilderCustom<HelpEntity> builder = new SpecificationsBuilderCustom<HelpEntity>();
		Pattern pattern = Pattern.compile(Constanst.QUERY_PATTERN);
		Matcher matcher = pattern.matcher(query + ",");
		Boolean isStatus = false;
		GetHelpsResponse getHelpsResponse = new GetHelpsResponse();
		// loop until to the last element of matcher
		while (matcher.find()) {
			// check if is exist key status and change value
			if (matcher.group(1).equals("status")) {
				isStatus = true;
			}
			builder.with(matcher.group(1), matcher.group(2), matcher.group(3));
		}
		// check if exist status
		if (isStatus) {
			Specification<HelpEntity> specification = builder.build();
			List<HelpEntity> helpEntities = helpRepo.findAll(specification);
			ModelMapper modelMapper = new ModelMapper();
			// loop until to the last element of list
			for (HelpEntity helpEntity : helpEntities) {
				getHelpsResponse.getHelp().add(modelMapper.map(helpEntity, Help.class));
			}
		}
		return getHelpsResponse;
	}

	/*
	 * method process logic and validate input value
	 */
	@Override
	public HelpResponse save(Help help) {
		HelpResponse helpResponse = new HelpResponse();
		HelpResult helpResult = new HelpResult();
		ModelMapper modelMapper = new ModelMapper();
		// check if help id not null
		if (help.getHelpId() != null) {
			helpResult.setHelpStatus(false); // set status
			helpResult.setHelpNotify("Add new can not need id"); // set message
		} else if (help.getHelpName() == null || help.getHelpName().trim().isEmpty()) { // check if name not null and
																						// not is empty
			helpResult.setHelpStatus(false);
			helpResult.setHelpNotify("Name of Help can not empty.");
		} else {
			// check if created user is null
			if (help.getCreatedUser() == null) {
				helpResult.setHelpStatus(false);
				helpResult.setHelpNotify("Created by can not null");
			} else {
				Optional<StaffEntity> staffOptional = staffRepo.findById(help.getCreatedUser().intValue());
				// check if created user is already exist
				if (staffOptional.isPresent()) {
					HelpEntity helpEntitySave = modelMapper.map(help, HelpEntity.class);
					helpEntitySave.setStaff(staffOptional.get());
					helpEntitySave.setStatus("1");
					HelpEntity helpEntity = helpRepo.save(helpEntitySave);
					// check if save done to database
					if (helpEntity.getHelpId() != null) {
						helpResult.setHelpStatus(true);
						helpResult.setHelpNotify("Add new Help done");
					} else {
						helpResult.setHelpStatus(false);
						helpResult.setHelpNotify("Add new can Help fail");
					}
				} else {
					helpResult.setHelpStatus(false);
					helpResult.setHelpNotify("Can not found created by id");
				}
			}
		}
		helpResponse.setHelp(helpResult);
		return helpResponse;
	}
	/*
	 * method update help and validate some field
	 * */
	@Override
	public HelpResponse update(Help help) {
		HelpResponse helpResponse = new HelpResponse();
		HelpResult helpResult = new HelpResult();
		ModelMapper modelMapper = new ModelMapper();
		// check if does not have id
		if (help.getHelpId() == null) {
			helpResult.setHelpStatus(false); // set status
			helpResult.setHelpNotify("Update help must have id"); // set message
		} else if (help.getHelpName() == null || help.getHelpName().trim().isEmpty()) {// check if name not null and not
																						// is empty
			helpResult.setHelpStatus(false);
			helpResult.setHelpNotify("Name of Help can not empty.");
		} else if (!("0".equals(help.getStatus()) || "1".equals(help.getStatus()))) {// check if status is not null and
																						// equal 1 or 0
			helpResult.setHelpStatus(false);
			helpResult.setHelpNotify("Status must right 0 or 1.");
		} else {
			// check if created user is null
			if (help.getCreatedUser() == null) {
				helpResult.setHelpStatus(false);
				helpResult.setHelpNotify("Created by can not null");
			} else {
				Optional<StaffEntity> staffOptional = staffRepo.findById(help.getCreatedUser().intValue());
				// check if created user is already exist
				if (staffOptional.isPresent()) {
					HelpEntity helpEntitySave = modelMapper.map(help, HelpEntity.class);
					helpEntitySave.setStaff(staffOptional.get());
					HelpEntity helpEntity = helpRepo.save(helpEntitySave);
					// check if save done to database
					if (helpEntity != null) {
						helpResult.setHelpStatus(true);
						helpResult.setHelpNotify("Update Help done");
					} else {
						helpResult.setHelpStatus(false);
						helpResult.setHelpNotify("Add new can Help fail");
					}
				} else {
					helpResult.setHelpStatus(false);
					helpResult.setHelpNotify("Can not found created by id");
				}
			}
		}
		helpResponse.setHelp(helpResult);
		return helpResponse;
	}
	/*
	 * method remove Help by id 
	 * */
	@Override
	public HelpResponse remove(RemoveHelp removeHelp) {
		HelpResponse helpResponse = new HelpResponse();
		HelpResult helpResult = new HelpResult();
		Optional<HelpEntity> heOptional = helpRepo.findById(removeHelp.getHelpId().intValue());
		// check if exist help by id
		if (heOptional.isPresent()) {
			heOptional.get().setStatus("0");
			helpRepo.save(heOptional.get());
			helpResult.setHelpStatus(true);
			helpResult.setHelpNotify("Delete Help done by id"+removeHelp.getHelpId());
		}else {
			helpResult.setHelpStatus(false);
			helpResult.setHelpNotify("Delete Help fail by id"+removeHelp.getHelpId());
		}
		helpResponse.setHelp(helpResult);
		return helpResponse;
	}

}

/*
 * %W% %E% staff-help - asus
 *
 * Copyright (c) 2017-2018 SMAC VNIT, jsc. All Rights Reserved. 
 *
 * This software is the confidential and proprietary information of SMAC VNIT, jsc. 
 * ("Confidential Information"). You shall not disclose such Confidential Information 
 * and shall use it only in accordance with the terms of the license agreement you entered 
 * into with SMAC.
 *
 * SMAC MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF 
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SMAC SHALL NOT BE LIABLE FOR 
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR 
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/
package com.staffhelp.endpoint.service.impl;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.staffhelp.endpoint.service.IStaffEndpointService;
import com.staffhelp.entity.StaffEntity;
import com.staffhelp.repo.IStaffRepository;
import com.staffhelp.specification.SpecificationsBuilderCustom;
import com.staffhelp.ultils.Constanst;

import io.spring.guides.gs_producing_web_service.GetStaffsResponse;
import io.spring.guides.gs_producing_web_service.Staff;

/**
 * Staff Service
 * 
 * @since 10:59:50 AM
 * @author asus
 */
@Service
public class StaffEndpointService implements IStaffEndpointService{
	@Autowired
	private IStaffRepository staffRepo;

	@Override
	public GetStaffsResponse findAll(String query) {
		// check if query is empty or null
		if(query == null || query.trim().isEmpty()) {
			return null;
		}
		SpecificationsBuilderCustom<StaffEntity> builder = new SpecificationsBuilderCustom<StaffEntity>();
		Pattern pattern = Pattern.compile(Constanst.QUERY_PATTERN);
		Matcher matcher = pattern.matcher(query + ",");
		Boolean isStatus = false;
		GetStaffsResponse getStaffsResponse = new GetStaffsResponse();
		// loop until to the last element of matcher
		while (matcher.find()) {
			// check if is exist key status and change value
			if (matcher.group(1).equals("status")) {
				isStatus = true;
			}
			builder.with(matcher.group(1), matcher.group(2), matcher.group(3));
		}
		// check if exist status
		if (isStatus) {
			Specification<StaffEntity> specification = builder.build();
			List<StaffEntity> staffEntities = staffRepo.findAll(specification);
			ModelMapper modelMapper = new ModelMapper();
			// loop until to the last element of list
			for (StaffEntity staffEntity : staffEntities) {
				getStaffsResponse.getStaff().add(modelMapper.map(staffEntity, Staff.class));
			}
		}
		return getStaffsResponse;
	}
	
}

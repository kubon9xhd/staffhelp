/*
 * %W% %E% staff-help - asus
 *
 * Copyright (c) 2017-2018 SMAC VNIT, jsc. All Rights Reserved. 
 *
 * This software is the confidential and proprietary information of SMAC VNIT, jsc. 
 * ("Confidential Information"). You shall not disclose such Confidential Information 
 * and shall use it only in accordance with the terms of the license agreement you entered 
 * into with SMAC.
 *
 * SMAC MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF 
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SMAC SHALL NOT BE LIABLE FOR 
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR 
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/
package com.staffhelp.endpoint.service;

import io.spring.guides.gs_producing_web_service.GetHelpsResponse;
import io.spring.guides.gs_producing_web_service.Help;
import io.spring.guides.gs_producing_web_service.HelpResponse;
import io.spring.guides.gs_producing_web_service.RemoveHelp;

/**
 * Class description goes here.
 * 
 * @since		10:55:14 AM
 * @author		asus
 */
public interface IHelpEndpointService {
	GetHelpsResponse findAll(String query);
	HelpResponse save(Help help);
	HelpResponse update(Help help);
	HelpResponse remove(RemoveHelp removeHelp);
}

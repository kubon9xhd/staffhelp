/*
 * %W% %E% staff-help - asus
 *
 * Copyright (c) 2017-2018 SMAC VNIT, jsc. All Rights Reserved. 
 *
 * This software is the confidential and proprietary information of SMAC VNIT, jsc. 
 * ("Confidential Information"). You shall not disclose such Confidential Information 
 * and shall use it only in accordance with the terms of the license agreement you entered 
 * into with SMAC.
 *
 * SMAC MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF 
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SMAC SHALL NOT BE LIABLE FOR 
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR 
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/
package com.staffhelp.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.staffhelp.endpoint.service.IStaffEndpointService;

import io.spring.guides.gs_producing_web_service.GetStaffsRequest;
import io.spring.guides.gs_producing_web_service.GetStaffsResponse;

/**
 * StaffEnpoint handle request
 * 
 * @since 9:58:40 AM
 * @author asus
 */
@Endpoint
public class StaffEndpoint {
	private static final String NAMESPACE_URI = "http://spring.io/guides/gs-producing-web-service";
	@Autowired
	private IStaffEndpointService staffService;
	/*
	 * Got request about criteria
	 * return list Staff form xml
	 * */
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetStaffsRequest")
	@ResponsePayload
	public GetStaffsResponse getHelpsResponse(@RequestPayload GetStaffsRequest request) {
		return staffService.findAll(request.getSearch());
	}
}
